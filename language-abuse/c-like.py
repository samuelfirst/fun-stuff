# The purpose of this is to force python to emulate c's
# syntax for delimiting blocks.  The end-result is far
# uglier, but mostly works.

# Ok, so it's not a perfect recreation of c's for loop
# structure, but it's close enough.  It's also a super
# hacky work-around, but it (kind of) gets the job done
def f0r(lower, upper, block):
    for i in range(lower, upper):
        eval(block)

bre = False
def wh1le(condition, block):
    global bre
    while eval(condition):
        if bre:
            bre = False
            break
        else:
            eval(block)

def br3ak():
    global bre
    bre = True

# The biggest issue with this is that you can't declare variables
# inside the blocks, which makes the work-arounds a bit more
# difficult.
def main(): {
    print('Hello World!'),

    f0r (0, 10, '''{
        print('ABC'),
    }'''),

    wh1le('True', '''{
        print('DEF'),
        br3ak(),
    }'''),
}

main()

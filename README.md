# Fun Stuff

A collection of useless/mildly interesting scripts that I've written for fun.

# Scripts:

## 1. self-modifying:
   * `selfBuildingScript.py`
     * This was my first attempt at a self-building script, it doesn't work, but I included it anyway for posterterity's sake
   * `selfBuildingScript_v2.py`
     * This was my second attempt at a self-building script, enter 'exit' at the prompt to exit (you might have to do it twice), otherwise, just hit enter and watch it grow (although, be careful, it grows exponentially).
   * `self-building-script.lisp`
     * Same concept as the python script, but it takes advantage of lisp's repl
   * `selfBuildingScript.sh`
     * Bash also uses repl (I mean, really it's more of a read-eval loop, but it serves the same purpose here)

## 2. quines:
   * `quine.lisp`
   * `quine.py`

## 3. polyglots:
   * `lish.lisp`
     * Valid as both common lisp and bash; prints a blank line
   * `lish_v2.lisp`
     * Builds on the concept of lish.lisp, but it actually prints something other than a blank line
   * `cthon.py`
     * C/Python polyglot; Loads entire main function using a C macro, and uses a multi-line comments to keep it from attempting to load the python part.

## 4. lanugage-abuse:
   * `c-like.py`
     * An abuse of python's dict syntax to make it more c-like

;;;; This works similarly to the python script, however,
;;;; using lisp allows us to take advantage of REPL

;; Define name of script and list containing script
(defvar *script-name* "self-building-script.lisp")
(defvar *script* nil)

;; Function to read script
(defun read-self ()
  (with-open-file (stream *script-name*)
		  (loop for line = (read-line stream nil)
			while line
			collect line)))

;; Function to write script to its self
(defun write-self ()
  (with-open-file (stream *script-name*
			 :direction :output
			 :if-exists :append)
		  (doList (line *script*)
			  (write-line line stream))))

;; Read script into *script*, write script, prompt to continue
(setf *script* (read-self))
(write-self)
(if (not (y-or-n-p "Continue?"))
    (quit))
(format t "Added layer")
